import axios from "axios";
import Crime from "../entities/Crime";

async function getCrimes(): Promise<Crime[]> {
  try {
    const response = await axios.get("http://139.162.157.192/api/get-crimes");
    const data = response.data;
    return data;
  } catch (error) {
    console.error("Error:", error)
    return [];
  }
}

export default getCrimes;
